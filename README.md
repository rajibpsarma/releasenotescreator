# Release Notes Creator

This is a tool that helps creating a consolidated release notes in .doc format, by reading a .csv file exported from JIRA that contains changes, enhancements and bug fixes details.

It's written in Java Swing.

## Screenshot
![Release Note Creator](src/main/resources/images/screenshot.png)

## Features ##
* Creates Release Notes in .doc format.
* Certain words can be removed from the issue summary.
* Reports issues that do not have a release note in JIRA.
* Automatically composes mails to respective teams missing release note in JIRA.
* Reports issues missing estimation.
* Allows configuring various things via properties file.

More details can be found [here](https://bitbucket.org/rajibpsarma/releasenotescreator/src/master/src/main/resources/docs/doc.txt)
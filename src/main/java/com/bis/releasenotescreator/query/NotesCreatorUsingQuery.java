package com.bis.releasenotescreator.query;

import com.bis.releasenotescreator.INotesCreator;
import com.bis.releasenotescreator.NotesCreationFailedException;
import com.bis.releasenotescreator.ui.Console;

/**
 * This class creates release notes using JIRA query
 * @author RSarma
 *
 */
public class NotesCreatorUsingQuery implements INotesCreator {

	/**
	 * creates release notes using JIRA query
	 */
	@Override
	public void createReleaseNotes() throws NotesCreationFailedException {
		Console.getInstance().write("Starting to create Release Notes by executing JIRA query ...");
		Console.getInstance().write("This is currently under development");
	}

}

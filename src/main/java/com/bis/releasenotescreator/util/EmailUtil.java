package com.bis.releasenotescreator.util;

import java.awt.Desktop;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bis.releasenotescreator.ui.Console;

/**
 * Utility class for email
 * @author RSarma
 */
public class EmailUtil {
	
	// The default subject, in case it's not specified in config.properties
	private static final String defaultSubject = "Missing Release Notes"; 
	// The default body,  in case it's not specified in config.properties
	private static final String defaultBody = 
			"Hi team <TEAM_NAME>,\n\n Release notes are missing for the following issues belonging to your team: \n     <ISSUES_MISSING_RELEASE_NOTES> \n\n Request to please enter them. \n\n Thanks ";
	
	/**
	 * Composes an email to each team having empty release notes and opens it in email editor.
	 * @param emptyRelNoteDetails Map: Key is sprint name, value is the list of issue id
	 */
	public static void composeEmailInEditor(Map<String, List<String>> emptyRelNoteDetails) {
		if(!emptyRelNoteDetails.isEmpty()) {
			// Check if Email functionality is needed
			String emailNeeded = getProperty("EMAIL_FUNC_NEEDED");
			if("N".equalsIgnoreCase(emailNeeded.toUpperCase())) {
				Console.getInstance().write("Not sending any Email.");
				return;
			}
			
			String teams = getProperty("SPRINT_TEAMS");
			if(teams == null) {
				return;
			}
			
			Map<String, List<String>> emptyReleaseNoteDetails = new HashMap<String, List<String>>(emptyRelNoteDetails);
			
			String[] teamNames = teams.split(",");
			String issueIdsWithEmptyReleaseNote = null;
			String emailIdOfTeam = null;
			for(String teamName : teamNames) {
				// sprintName is something like "2019.2 sprint 5 Red".
				// teamName is something like "Red".
				issueIdsWithEmptyReleaseNote = getEmptyReleaseNoteIssuesOfTeam(
						teamName, emptyReleaseNoteDetails);
				if(issueIdsWithEmptyReleaseNote != null) {
					// This team has issues with empty release notes. Read the email ids of the team.
					emailIdOfTeam = getEmailIdOfTeam(teamName);
					if(emailIdOfTeam != null) {
						composeEmail(teamName, emailIdOfTeam, issueIdsWithEmptyReleaseNote);
					}
				}
			}
			
			// Compose mails for all remainings e.g. Miscellaneous, empty sprint etc
			Iterator<String> itr = emptyReleaseNoteDetails.keySet().iterator();
			while(itr.hasNext()) {
				String sprintName = itr.next();
				if(!isAlreadyConsidered(teamNames, sprintName)) {
					List<String> issues = emptyReleaseNoteDetails.get(sprintName);
					if((issues != null) && (!issues.isEmpty())) {
						composeEmail(sprintName, "", StringUtils.getAsString(issues));
					}
				}
			}
		}
	}
	
	/**
	 * Checks if the sprint is already considered for composing email 
	 * @param teamNames
	 * @param sprintName
	 * @return
	 */
	private static boolean isAlreadyConsidered(String[] teamNames, String sprintName) {
		boolean result = false;
		
		for(String teamName : teamNames) {
			if(sprintName.contains(teamName)) {
				result = true;
				break;
			}
		}
		
		return result;
	}
	
	/**
	 * Composes a mail to a team
	 * @param teamName The name of the team
	 * @param emailIds The mail ids of the members of the team.
	 * @param issueIds The issue ids having empty release notes
	 */
	private static void composeEmail(String teamName, String emailIds, String issueIds) {
		try {
			Console.getInstance().write("Composing email for team '" + teamName + "' ...");
			// Get email subject, use the default one if not specified in properties file
			String subject = getProperty("EMAIL_SUBJECT"); 
			if(subject == null) {
				subject = defaultSubject;
			}
			subject = urlEncode(subject);
			
			// Get email body, use the default one if not specified in properties file
			String body = getProperty("EMAIL_BODY");
			if(body == null) {
				body = defaultBody;
			}
			body = body.replace("<TEAM_NAME>", teamName);
			body = body.replace("<ISSUES_MISSING_RELEASE_NOTES>", issueIds);
			body = urlEncode(body);
			
			String emails = urlEncode(emailIds);
			
			String uriStr = String.format("mailto:%s?subject=%s&body=%s", emails, subject, body);
			
			Desktop.getDesktop().browse(new URI(uriStr));
			Console.getInstance().write("Done.");
		}
		catch(Exception ex) {
			Console.getInstance().write("Error while composing email for team '" + teamName + "'. The details :");
			Console.getInstance().write(ex.getMessage());
			Console.getInstance().write(ExceptionUtil.getStackTraceAsString(ex));
		}
	}
	
	private static String urlEncode(String str) throws UnsupportedEncodingException {
		return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
	}
	
	private static String getProperty(String key) {
		String val = ConfigReader.getProperty(key);
		if(val == null) {
			Console.getInstance().write("Please set the value for '" + key + "' in config.properties");
		}
		return val;
	}
	
	/**
	 * Returns the issues with empty release notes for a team, or null.
	 * @param teamName
	 * @param emptyReleaseNoteDetails
	 * @return
	 */
	private static String getEmptyReleaseNoteIssuesOfTeam(String teamName, Map<String, List<String>> emptyReleaseNoteDetails) {
		String issues = null;
		List<String> issueIds = new ArrayList<>();
		Set<String> sprintNames = emptyReleaseNoteDetails.keySet();
		
		// Get issue ids of the team
		for(String sprintName : sprintNames) {
			if((sprintName != null) && (sprintName.contains(teamName))) {
				List<String> tempIssueIds = emptyReleaseNoteDetails.get(sprintName);
				if((tempIssueIds != null) && (!tempIssueIds.isEmpty())) {
					issueIds.addAll(tempIssueIds);
				}
			}
		}
		
		if((issueIds != null) && (!issueIds.isEmpty())) {
			issues = StringUtils.getAsString(issueIds);
		}
		
		return issues;
	}
	
	/**
	 * Returns the email ids configured for a team.
	 * @param teamName
	 * @return
	 */
	private static String getEmailIdOfTeam(String teamName) {
		String emailId = null;
		
		String emailIdKey = "EMAIL_ID_TEAM_" + teamName.toUpperCase();
		emailId = getProperty(emailIdKey);
		
		return emailId;
	}
}
package com.bis.releasenotescreator.util;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Util class, UI related.
 * @author RSarma
 */
public class UIUtil {
	/**
	 * Moves a container to the center of the screen
	 * @param paramContainer
	 */
	public static void moveToCenterOfScreen(Container paramContainer) {
	    double d1 = paramContainer.getWidth();
	    double d2 = paramContainer.getHeight();
	    int i = 0;
	    int j = 0;
	    Toolkit localToolkit = Toolkit.getDefaultToolkit();
	    Dimension localDimension = localToolkit.getScreenSize();
	    double d3 = localDimension.getWidth();
	    double d4 = localDimension.getHeight();
	    i = (int)((d3 - d1) / 2.0D);
	    j = (int)((d4 - d2) / 2.0D);
	    paramContainer.setLocation(i, j);
	}	
}

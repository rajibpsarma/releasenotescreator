package com.bis.releasenotescreator.util;

/**
 * @author RSarma
 */
public class ExceptionUtil {
	/**
	 * Returns the stack trace as string.
	 * @param e The exception object
	 * @return The stack trace as string.
	 */
	public static String getStackTraceAsString(Throwable e) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : e.getStackTrace()) {
			sb.append(element.toString());
			sb.append("\n     ");
		}
		return sb.toString();
	}
}

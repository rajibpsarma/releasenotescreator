package com.bis.releasenotescreator.util;

import java.util.List;

public class StringUtils {
	/**
	 * Returns a list elements as comma separated string
	 * @param list
	 * @return
	 */
	public static String getAsString(List<String> list) {
		StringBuilder str = new StringBuilder();
		for(int index = 0; index < list.size(); index++) {
			str.append(list.get(index));
			if(index < (list.size() - 1)) {
				str.append(",");
			}
		}
		return str.toString();
	}

}

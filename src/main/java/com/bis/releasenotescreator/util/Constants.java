package com.bis.releasenotescreator.util;

import java.awt.Color;
import java.awt.Font;

/**
 * Constants
 * @author RSarma
 */
public class Constants {
	public static Font FONT_NORMAL = new Font("Callibri", Font.PLAIN, 20);
	public static Font FONT_HEADER = new Font("Callibri", Font.BOLD, 20);
	public static Font FONT_CONSOLE = new Font("Callibri", Font.PLAIN, 14);
	public static Font FONT_HELP_DIALOG = new Font("Callibri", Font.PLAIN, 16);
	
	public static Color COLOUR_BACKGROUND = Color.decode("#E5E8FC");//Color.decode("#ccddff");
	public static Color BUTTON_BACKGROUND = Color.decode("#0C25CC");//Color.decode("#ccddff");
	
	static int SCREEN_WIDTH = 200;
	
	public static int NOTES_CREATOR_OPTION_JIRA_QUERY = 1;
	public static int NOTES_CREATOR_OPTION_CSV_FILE = 2;
}

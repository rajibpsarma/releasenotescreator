package com.bis.releasenotescreator.util;

import java.io.IOException;
import java.util.Properties;

import com.bis.releasenotescreator.ui.Console;

/**
 * @author RSarma
 */
public class ConfigReader {
	private static Properties props = null;
	
	static {
		if(props == null) {
			props = new Properties();
			try {
				props.load(ConfigReader.class.getClassLoader().getResourceAsStream("config.properties"));
			}
			catch(IOException ex) {
				Console.getInstance().write("Error reading 'config.properties' file :");
				Console.getInstance().write(ex.getMessage());
				Console.getInstance().write(ExceptionUtil.getStackTraceAsString(ex));
			}
		}
	}
	
	/**
	 * Returns a property value for the key
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		String val = null;
		if(props != null) {
			val = props.getProperty(key);
		}
		return val;
	}
}

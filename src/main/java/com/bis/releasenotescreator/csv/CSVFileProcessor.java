package com.bis.releasenotescreator.csv;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.bis.releasenotescreator.ui.Console;
import com.bis.releasenotescreator.util.ConfigReader;
import com.bis.releasenotescreator.util.ExceptionUtil;
import com.bis.releasenotescreator.util.StringUtils;
import com.opencsv.CSVReader;

/**
 * It processes the CSV file and extracts the required data.
 * @author RSarma
 */
public class CSVFileProcessor {
	// Words to remove from the description
	private String[] wordsToRemove = null;
	// The list of index of column names of the CSV file that are needed to generate the release notes document.
	private List<Integer> columnIndicesNeededForRelNotesDoc = new ArrayList<>();
	// The column index of issue id column
	private int columnIndexIssueId =  -1;
	// The column index of Summary column
	private int columnIndexSummary =  -1;
	// The column index of release note column
	private int columnIndexReleaseNote =  -1;
	// The column index of Story Point column
	private int columnIndexStoryPoint =  -1;
	// The column index of Sprint column
	private int columnIndexSprint =  -1;
	// This map stores the issue ids that do not have a release note, for a sprint.
	// Key is sprint name, value is the list of issue id
	Map<String, List<String>> emptyReleaseNotesMap = new HashMap<>();
	// Stores all the issue ids that do not have an estimate
	List<String> unestimatedIssues = new ArrayList<>();
	
	public CSVFileProcessor() {
		String errorMsg = "Please set the value of WORDS_TO_REMOVE_FROM_DESCRIPTION in config.properties";
		String strWordsToRemove = getCsvFileColumnName("WORDS_TO_REMOVE_FROM_DESCRIPTION");
		if(strWordsToRemove != null) {
			wordsToRemove = strWordsToRemove.split(",");
			if(wordsToRemove.length == 0) {
				Console.getInstance().write(errorMsg);	
			}
		}
	}
	
	/**
	 * Processes a  CSV file and returns the required data as rows and columns.
	 * @param csvFile
	 * @return the required data as rows and columns.
	 */
	public List<List<String>> processCSV(String csvFile) {
		// Some error messages
		String errorMsgNoDataFound = "No data found in file \"" + csvFile + "\"";
		
		List<List<String>> data = new ArrayList<List<String>>();
		
		Console.getInstance().write("Processing \"" + csvFile + "\" ...");
		
		CSVReader reader = null;
		try {
			// Read the entire data from the CSV file.
			reader = new CSVReader(new FileReader(csvFile));
			if(reader != null) {
				List<String[]> csvData = reader.readAll();
				if(csvData != null) {
					int csvRowCount = csvData.size();
					if(csvRowCount == 0) {
						// No data in CSV file
						Console.getInstance().write(errorMsgNoDataFound);
						throw new CSVProcessorException();							
					}
					if(csvData.get(0) != null) {
						int csvColumnCount = csvData.get(0).length;
						if(csvColumnCount == 0) {
							// No data in CSV file
							Console.getInstance().write(errorMsgNoDataFound);
							throw new CSVProcessorException();
						}
						
						// Process the CSV data and filter out what is needed for us.
						int csvDataRowIndex = 0; List<String> rowData = new ArrayList<>();
						for(String[] csvRowData : csvData) {
							
							// The first row contains headers, use it to determine the columns we need to use.
							if(csvDataRowIndex == 0) {
								// Populate various column indices to be used later, using the header names.
								populateColumnIndices(csvRowData);
								++csvDataRowIndex;
								continue;
							}
							// Populate the data needed for release notes document.
							rowData = new ArrayList<>();
							for(int colIndex : columnIndicesNeededForRelNotesDoc) {
								// For Summary, remove the unnecessary words
								if(colIndex == columnIndexSummary) {
									rowData.add(removeUnnecessaryWords(csvRowData[colIndex]));									
								}
								else {
									String colData = csvRowData[colIndex];
									if(colData == null) {
										colData = " ";
									}
									rowData.add(colData);	
								}
							}
							data.add(rowData);
							
							// Check if the release note is empty. If so, store the required data
							checkEmptyReleaseNotes(csvRowData);
							
							// Check if the issue is not estimated. If so, store the required data
							checkStoryPoints(csvRowData);
						}
						Console.getInstance().write("The file \"" + csvFile + "\" processed successfully");
						Console.getInstance().write(data.size() + " records found.");
					} else {
						// No data in CSV file
						Console.getInstance().write(errorMsgNoDataFound);
						throw new CSVProcessorException();
					}
				}
				else {
					// No data in CSV file
					Console.getInstance().write(errorMsgNoDataFound);
					throw new CSVProcessorException();
				}
			}
		}
		catch(CSVProcessorException ex) {
			// The exact error message was already written to console.
			Console.getInstance().write("Please fix the above error and try again.");
		}
		catch(Exception e) {
			Console.getInstance().write("Error while reading data from \"" + csvFile + "\" :");
			Console.getInstance().write(e.getMessage());
			Console.getInstance().write(ExceptionUtil.getStackTraceAsString(e));
		}
		finally {
			if(reader != null) {
				try {
					reader.close();
				} catch(IOException ex){}
			}
		}
		
		return data;
	}
	
	/**
	 * prints the details about empty release notes and unestimated stories
	 */
	public void printFindings() {
		Console.getInstance().write("\n ------------------------------ FINDINGS ------------------------------");
		
		// Print details of empty release notes
		if(!emptyReleaseNotesMap.isEmpty()) {
			Console.getInstance().write("Release Notes are not available for the followings :");
			Iterator<String> keys = emptyReleaseNotesMap.keySet().iterator();
			String sprintName = "";
			List<String> issueList = null;
			String issueIdStr = "";
			int index = 1;
			while(keys.hasNext()) {
				sprintName = keys.next();
				issueList = emptyReleaseNotesMap.get(sprintName);
				if(issueList == null) {
					issueIdStr = "None";
				}
				else {
					issueIdStr = StringUtils.getAsString(issueList);
				}
				if(sprintName != null) {
					Console.getInstance().write("   " + index + ") For Sprint : " + sprintName);
					Console.getInstance().write("      Issue IDs : " + issueIdStr);
					++index;
				}
			}
		}
		else {
			// All release notes available
			Console.getInstance().write("Release Notes are available for all the Issues.");
		}
		
		// Print details of unestimated issues
		if(!unestimatedIssues.isEmpty()) {
			Console.getInstance().write("\n The following issues are not Estimated yet :");
			Console.getInstance().write("   " + StringUtils.getAsString(unestimatedIssues));
		}
		else {
			// All issues estimated
			Console.getInstance().write("All the Issues are Estimated.");
		}
		
		Console.getInstance().write("----------------------------------------------------------------------");
	}
	
	/**
	 * Checks if a valid release notes is available. If not, it stores the issue id against the sprint.
	 * @param csvRowData The CSV file row data
	 */
	private void checkEmptyReleaseNotes(String[] csvRowData) {
		if(columnIndexReleaseNote !=  -1) {
			String relNotes = csvRowData[columnIndexReleaseNote];
			if(!isReleaseNoteAvailable(relNotes)) {
				// Store the issue id against the sprint name.
				if((columnIndexIssueId !=  -1) && (columnIndexSprint !=  -1)) {
					String issueId = csvRowData[columnIndexIssueId];
					String sprintName = csvRowData[columnIndexSprint];
					if((issueId != null) && (sprintName != null)) {
						List<String> issueIdsForSprintName = emptyReleaseNotesMap.get(sprintName);
						if(issueIdsForSprintName == null) {
							issueIdsForSprintName = new ArrayList<>();
						}
						issueIdsForSprintName.add(issueId);
						emptyReleaseNotesMap.put(sprintName, issueIdsForSprintName);
					}
				}
			}
		}
	}
	
	/**
	 * Checks if a valid story point is available. If not, it stores the issue id.
	 * @param csvRowData The CSV file row data
	 */
	private void checkStoryPoints(String[] csvRowData) {
		if(columnIndexStoryPoint !=  -1) {
			String storyPoint = csvRowData[columnIndexStoryPoint];
			if(!isStoryPointAvailable(storyPoint)) {
				// Store the issue id.
				if(columnIndexIssueId !=  -1) {
					String issueId = csvRowData[columnIndexIssueId];
					if(issueId != null) {
						unestimatedIssues.add(issueId);
					}
				}
			}
		}
	}
	
	private boolean isReleaseNoteAvailable(String releaseNoteText) {
		if(releaseNoteText == null) {
			return false;
		}
		if(releaseNoteText.trim().isEmpty()) {
			return false;
		}
		return true;
	}

	private boolean isStoryPointAvailable(String storyPointText) {
		if(storyPointText == null) {
			return false;
		}
		if(storyPointText.trim().isEmpty()) {
			return false;
		}
		if(storyPointText.trim().equals("0")) {
			return false;
		}
		return true;
	}
	
	/**
	 * Populates the index of various columns of the CSV files in the class level variables. They are used in this class.
	 * @return
	 */
	private void populateColumnIndices(String[] csvRowHeaderData) {
		List<String> csvHeaderNames = Arrays.asList(csvRowHeaderData);

		// Populate columnIndicesNeededForRelNotesDoc
		String errorMsg = "Please set the value of JIRA_CSV_FILE_COLUMNS_NEEDED_FOR_REL_NOTES in config.properties";
		String csvFileColumnNames = ConfigReader.getProperty("JIRA_CSV_FILE_COLUMNS_NEEDED_FOR_REL_NOTES");
		if(csvFileColumnNames != null) {
			String[] columnNamesToBeRead = csvFileColumnNames.split(",");
			if(columnNamesToBeRead.length == 0) {
				Console.getInstance().write(errorMsg);	
			}
			else {
				// Get the index of the columns needed in the release note
				for(String colName : columnNamesToBeRead) {
					columnIndicesNeededForRelNotesDoc.add(csvHeaderNames.indexOf(colName));
				}
			}
		}
		else {
			Console.getInstance().write(errorMsg);
		}
		
		// populate column index of issue id
		String csvFileColumnName = getCsvFileColumnName("JIRA_CSV_FILE_COLUMN_ISSUE_ID");
		if(csvFileColumnName != null) {
			columnIndexIssueId = csvHeaderNames.indexOf(csvFileColumnName);
		}
		
		// Populate column index of Summary
		csvFileColumnName = getCsvFileColumnName("JIRA_CSV_FILE_COLUMN_SUMMARY");
		if(csvFileColumnName != null) {
			columnIndexSummary = csvHeaderNames.indexOf(csvFileColumnName);
		}
		
		// populate column index of release note
		csvFileColumnName = getCsvFileColumnName("JIRA_CSV_FILE_COLUMN_RELEASE_NOTE");
		if(csvFileColumnName != null) {
			columnIndexReleaseNote = csvHeaderNames.indexOf(csvFileColumnName);
		}
		
		// populate column index of Story Point
		csvFileColumnName = getCsvFileColumnName("JIRA_CSV_FILE_COLUMN_STORY_POINT");
		if(csvFileColumnName != null) {
			columnIndexStoryPoint = csvHeaderNames.indexOf(csvFileColumnName);
		}
		
		// populate column index of Sprint
		csvFileColumnName = getCsvFileColumnName("JIRA_CSV_FILE_COLUMN_SPRINT");
		if(csvFileColumnName != null) {
			columnIndexSprint = csvHeaderNames.indexOf(csvFileColumnName);
		}
	}
	
	/**
	 * Reads the column name using the key, from config.properties.
	 * @param key
	 * @return
	 */
	private String getCsvFileColumnName(String key) {
		String csvFileColumnName = null;
		csvFileColumnName = ConfigReader.getProperty(key);
		
		String errorMsg = "Please set the value of " + key + " in config.properties";
		if(csvFileColumnName == null) {
			Console.getInstance().write(errorMsg);
			return null;
		}
		if(csvFileColumnName.trim().equals("")) {
			Console.getInstance().write(errorMsg);
			return null;
		}
		
		return csvFileColumnName;
	}
	
	/**
	 * Removes unnecessary words from summary
	 * @param columnData
	 * @return
	 */
	private String removeUnnecessaryWords(String columnData) {
		int pos =  -1;
		
		if(wordsToRemove != null) {
			for(String wordToRemove : wordsToRemove) {
				pos = columnData.indexOf(wordToRemove, 0);
				if(pos > -1) {
					columnData = columnData.substring(pos + wordToRemove.length());
				}
			}			
		}

		return columnData;
	}
	
	/**
	 * Returns the empty release note details
	 * @return Map: Key is sprint name, value is the list of issue id
	 */
	public Map<String, List<String>> getEmptyReleaseNoteDetails() {
		return this.emptyReleaseNotesMap;
	}
	
	private class CSVProcessorException extends Exception {
	}
}

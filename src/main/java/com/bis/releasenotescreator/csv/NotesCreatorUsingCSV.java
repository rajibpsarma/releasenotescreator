package com.bis.releasenotescreator.csv;

import java.util.List;

import javax.swing.JFileChooser;

import com.bis.releasenotescreator.INotesCreator;
import com.bis.releasenotescreator.NotesCreationFailedException;
import com.bis.releasenotescreator.creators.DocumentGenerator;
import com.bis.releasenotescreator.ui.Console;
import com.bis.releasenotescreator.util.ConfigReader;
import com.bis.releasenotescreator.util.Constants;
import com.bis.releasenotescreator.util.EmailUtil;

/**
 * This class creates release notes by uploading CSV file
 * @author RSarma
 *
 */
public class NotesCreatorUsingCSV implements INotesCreator {
	/**
	 * creates release notes by uploading CSV file
	 */
	@Override
	public void createReleaseNotes() throws NotesCreationFailedException {
		Console.getInstance().write("Starting to create Release Notes by uploading CSV file ...");
		
		// Get the CSV file path, that JIRA downloads the CSV to.
		String csvPath = ConfigReader.getProperty("JIRA_CSV_DOWNLOAD_PATH");
		String userName = System.getProperty("user.name");
		csvPath = csvPath.replace("<USER_NAME>", userName);
		
		// Allow the user to choose the CSV file to upload
		JFileChooser fileChooser = new JFileChooser(csvPath);
		fileChooser.setFont(Constants.FONT_NORMAL);
		fileChooser.setBackground(Constants.COLOUR_BACKGROUND);
		fileChooser.setDialogTitle("Select the CSV file containing JIRA release note details");
		fileChooser.showOpenDialog(Console.getInstance().getUserInterface());
		if(fileChooser.getSelectedFile() != null) {
			// A file is selected by the user
			String csvFileChoosen = fileChooser.getSelectedFile().getAbsolutePath();
			// Process the CSV file selected by the user
			CSVFileProcessor csvProcessor = new CSVFileProcessor();
			List<List<String>> data = csvProcessor.processCSV(csvFileChoosen);
			// Create the release notes doc file using the data
			if(!data.isEmpty()) {
				new DocumentGenerator().generateDocFile(data);
			}
			
			// Display the findings in the console.
			csvProcessor.printFindings();
			
			// Compose emails for missing release notes
			EmailUtil.composeEmailInEditor(csvProcessor.getEmptyReleaseNoteDetails());
		}
		else {
			// No CSV file selected
			Console.getInstance().write("You have not chosen the CSV file");
		}
	}
}

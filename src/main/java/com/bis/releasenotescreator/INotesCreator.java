package com.bis.releasenotescreator;

/**
 * Interface that is implemented by 2 classes.
 * @author RSarma
 *
 */
public interface INotesCreator {
	// Creates a release notes.
	public void createReleaseNotes() throws NotesCreationFailedException;
}

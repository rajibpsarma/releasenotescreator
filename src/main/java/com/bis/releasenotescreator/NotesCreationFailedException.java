package com.bis.releasenotescreator;

/**
 * @author RSarma
 */
public class NotesCreationFailedException extends Exception {
	public NotesCreationFailedException(Exception ex) {
		super(ex);
	}
}

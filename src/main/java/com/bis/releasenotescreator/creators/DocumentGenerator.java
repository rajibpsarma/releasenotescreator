package com.bis.releasenotescreator.creators;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import javax.swing.JOptionPane;

import com.bis.releasenotescreator.ui.Console;
import com.bis.releasenotescreator.util.ConfigReader;
import com.bis.releasenotescreator.util.ExceptionUtil;

import word.api.interfaces.IDocument;
import word.w2004.Document2004;
import word.w2004.elements.Table;
import word.w2004.elements.tableElements.TableEle;

/**
 * It creates the .doc file with the release note contents.
 * @author RSarma
 *
 */
public class DocumentGenerator {
	public void generateDocFile(List<List<String>> data) {
		if(data.isEmpty()) {
			Console.getInstance().write("No data is available to create the release notes .doc file. Quiting ...");
			return;
		}
		
		String fileName = ConfigReader.getProperty("DOC_FILE_PATH");
		String userName = System.getProperty("user.name");
		fileName = fileName.replace("<USER_NAME>", userName);
		
		Console.getInstance().write("Generating file \"" + fileName + "\" ...");
		
		// Check the file to be generated
		File file = new File(fileName);
		if(file.exists()) {
			// The file is already available. Ask the user if needs to be overwritten.
			Console.getInstance().write("The file \"" + fileName + "\" already exists");
			int option = JOptionPane.showConfirmDialog(Console.getInstance().getUserInterface(), 
					"The file already exists. Do you want to overwrite it ?\n" + fileName, "The file already exists, overwrite ?",
					JOptionPane.YES_NO_OPTION);			
			if(JOptionPane.YES_OPTION == option) {
				// The user chooses to overwrite.
				Console.getInstance().write("Overwriting file \"" + fileName + "\" ...");
				file.delete();
			}
			else {
				Console.getInstance().write("The user chooses not to overwrite file \"" + fileName + "\".");
				Console.getInstance().write("File generation aborted.");
				return;
			}
		}
		
		// Fetch the release note table headers
		String[] headerArr = null;
		String msgNoHeaders = "Please set release note table headers in RELEASE_NOTE_TABLE_HEADERS of config.properties";
		String headers = ConfigReader.getProperty("RELEASE_NOTE_TABLE_HEADERS");
		if(headers != null) {
			headerArr = headers.split(",");
			if(headerArr.length == 0) {
				Console.getInstance().write(msgNoHeaders);
			}
		} else {
			Console.getInstance().write(msgNoHeaders);
		}
		
		// Create the release notes doc file
		FileWriter writer = null;
		try {
			IDocument myDoc = new Document2004();
			Table tbl = new Table();
			// Add table headers
			tbl.addTableEle(TableEle.TD, headerArr);
			for(List<String> rowData : data) {
				// Add table row data
				String[] rowDataArr = new String[rowData.size()];
				rowDataArr = rowData.toArray(rowDataArr);
				tbl.addTableEle(TableEle.TD, rowDataArr);
			}
			myDoc.addEle(tbl);
		    // Get the document contents and save as file.
			String myWord = myDoc.getContent();
			writer = new FileWriter(file);
			writer.write(myWord);
			Console.getInstance().write("The file \"" + fileName + "\" generated successfully.");
		}
		catch(Exception ex) {
			Console.getInstance().write("Error while creating file \"" + fileName + "\" :");
			Console.getInstance().write(ex.getMessage());
			Console.getInstance().write(ExceptionUtil.getStackTraceAsString(ex));
		}
		finally {
			try { if(writer != null)writer.close();}
			catch(Exception ex){}
		}
	}
	
	// Just for testing
	public static void main(String[] args) {
		new DocumentGenerator().generateDocFile(null);
	}
}

package com.bis.releasenotescreator;

import com.bis.releasenotescreator.csv.NotesCreatorUsingCSV;
import com.bis.releasenotescreator.query.NotesCreatorUsingQuery;
import com.bis.releasenotescreator.util.Constants;

/**
 * Factory class for getting the actual instance of Notes Creator
 * @author RSarma
 */
public class NotesCreatorFactory {
	/**
	 * Returns the instance of notes creator based on  notesCreatorOption
	 * @param notesCreatorOption
	 * @return
	 */
	public static INotesCreator getNotesCreator(int notesCreatorOption) {
		INotesCreator notesCreator = null;
		
		if(notesCreatorOption == Constants.NOTES_CREATOR_OPTION_JIRA_QUERY) {
			notesCreator = new NotesCreatorUsingQuery();
		}
		else {
			notesCreator = new NotesCreatorUsingCSV();
		}
		
		return notesCreator;
	}
}

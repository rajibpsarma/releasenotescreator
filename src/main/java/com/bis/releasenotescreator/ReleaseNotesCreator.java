package com.bis.releasenotescreator;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.UIManager;

import com.bis.releasenotescreator.ui.Console;
import com.bis.releasenotescreator.ui.OptionsPane;
import com.bis.releasenotescreator.util.Constants;
import com.bis.releasenotescreator.util.UIUtil;

/**
 * The main class, the starting point.
 * @author RSarma
 */
public class ReleaseNotesCreator extends WindowAdapter {
	public static void main(String args[]) {
		new ReleaseNotesCreator().init();
	}
	
	private void init() {
		JFrame frame = new JFrame("Release Notes Creator");
		// Display the icon in title
		try {
			frame.setIconImage(ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("images/logo.png")));
		} 
		catch(Exception e){}
		
		// Add UI elements
		Container localContainer = frame.getContentPane();
	    localContainer.setLayout(new FlowLayout());
	    
	    Box localBox = Box.createVerticalBox();
	    localBox.add(new OptionsPane().getUserInterface());
	    localBox.add(Console.getInstance().getUserInterface());
	    localContainer.add(localBox);
	    
	    // Add styles
	    frame.setFont(Constants.FONT_HEADER);
	    
	    frame.pack();
	    UIUtil.moveToCenterOfScreen(frame);
	    frame.setVisible(true);
	    frame.setResizable(false);
	    frame.addWindowListener(this);
	    
	    // Set look and feel
	    try {
	    	UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
	    catch(Exception ex){}
	}
	
	public void windowClosing(WindowEvent paramWindowEvent) {
		System.exit(0);
	}	
}
package com.bis.releasenotescreator.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.bis.releasenotescreator.INotesCreator;
import com.bis.releasenotescreator.NotesCreationFailedException;
import com.bis.releasenotescreator.NotesCreatorFactory;
import com.bis.releasenotescreator.util.Constants;

/**
 * Class representing the pane that allows the user to select options and start the creation process.
 * @author RSarma
 *
 */
public class OptionsPane {
	// The main container that holds all other elements in it.
	private JPanel paneUserInterface = new JPanel();
	
	// The option selected for creating release notes, 
	// i.e. Constants.NOTES_CREATOR_OPTION_JIRA_QUERY or Constants.NOTES_CREATOR_OPTION_CSV_FILE
	private int relNotesCreationOption = Constants.NOTES_CREATOR_OPTION_CSV_FILE;
	
	public OptionsPane() {
		init();
	}
	
	/**
	 * Returns the user interface 
	 * @return The user interface 
	 */
	public JPanel getUserInterface() {
		return paneUserInterface;
	}
	
	private void init() {
		JPanel p = new JPanel();
		Box box = Box.createVerticalBox();
		p.add(box);
		
		// Add the UI elements
		JLabel emptySpace = new JLabel(" ");
		JLabel txt = new JLabel("Creates Release Notes by using a CSV file                        ");
		JPanel buttonPanel = new JPanel();
		Box btnPanelBox = Box.createHorizontalBox();
		JButton btnCreate = new JButton("Create");
		JButton btnHelp = new JButton("Help");
		btnPanelBox.add(btnCreate);
		btnPanelBox.add(new JLabel("   ")); // Some empty space
		btnPanelBox.add(btnHelp);
		buttonPanel.add(btnPanelBox);
		box.add(txt);
		box.add(emptySpace);
		box.add(buttonPanel);
		
		// Set various styles
		txt.setBackground(Constants.COLOUR_BACKGROUND);
		txt.setForeground(Color.black);
		txt.setFont(Constants.FONT_NORMAL);
		emptySpace.setPreferredSize(new Dimension(10, 10));
		buttonPanel.setBackground(Constants.COLOUR_BACKGROUND);
		btnCreate.setFont(Constants.FONT_NORMAL);
		btnCreate.setFocusable(false);
		btnCreate.setToolTipText("Creates Release Notes by using a CSV file downloaded from JIRA");
		btnHelp.setFont(Constants.FONT_NORMAL);
		btnHelp.setFocusable(false);
		btnHelp.setToolTipText("Displays the User Guide");
		p.setBackground(Constants.COLOUR_BACKGROUND);
		paneUserInterface.add(p);
		paneUserInterface.setBorder(BorderFactory.createEtchedBorder());
		paneUserInterface.setBackground(Constants.COLOUR_BACKGROUND);
		
		// Handle events
		btnCreate.addActionListener(new CreateButtonListener());
		btnHelp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				HelpDialog.show();
			}
		});
	}
	
	// The listener class for the Create button.
	// It's invoked when Create button is clicked.
	private class CreateButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent ae) {
			// Clear console
			Console.getInstance().clear();
			// Get the instance of the Notes Creator, based on the option selected.
			INotesCreator notesCreator = NotesCreatorFactory.getNotesCreator(relNotesCreationOption);
			// Now, try creating release notes.
			try {
				notesCreator.createReleaseNotes();
			}
			catch(NotesCreationFailedException ex) {
				// Write the error details to console
				Console.getInstance().write("Release Notes creation failed due to the following error :");
				Console.getInstance().write(ex.getMessage());
			}
		}
	}
}
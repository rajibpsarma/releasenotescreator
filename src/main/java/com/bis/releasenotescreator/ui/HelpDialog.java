package com.bis.releasenotescreator.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.bis.releasenotescreator.util.Constants;
import com.bis.releasenotescreator.util.UIUtil;

/**
 * It displays the help dialog.
 * @author RSarma
 *
 */
public class HelpDialog {
	private static JDialog dialog = new JDialog();
	
	/**
	 * Shows the help dialog
	 */
	public static void show() {
		// Display the icon in title
		try {
			dialog.setIconImage(ImageIO.read(HelpDialog.class.getClassLoader().getResourceAsStream("images/logo.png")));
		} 
		catch(Exception e){}
		
		// Add various UI components
		Container localContainer = dialog.getContentPane();
		localContainer.setLayout(new BorderLayout());
		JScrollPane localJScrollPane = new JScrollPane();
		localJScrollPane.getViewport().add(getTextField(), "Center");
		localContainer.add(localJScrollPane, "Center");
		localContainer.add(getCloseButton(), "South");
	    
		
		// Set various properties
		dialog.setTitle("Release Notes Creator : User Documentation");
		dialog.setBackground(Constants.COLOUR_BACKGROUND);
		dialog.setModal(false);
		//dialog.pack();
		dialog.setSize(730, 500);
		UIUtil.moveToCenterOfScreen(dialog);
		dialog.setVisible(true);
	}
	
	/**
	 * Builds and returns the text field.
	 * @return
	 */
	private static JEditorPane getTextField() {
		JEditorPane pane = new JEditorPane();
		// Read text to be displayed
		try {
			pane.read(HelpDialog.class.getClassLoader().getResourceAsStream("docs/doc.txt"), null);
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
		
		pane.setEditable(false);
		pane.setFont(Constants.FONT_HELP_DIALOG);
		
		return pane;
	}
	
	/**
	 * Builds and returns the Close button
	 * @return
	 */
	private static JPanel getCloseButton() {
		JButton btn = new JButton("Close");
		btn.setFocusable(false);
		btn.setFont(Constants.FONT_NORMAL);
		btn.setBackground(Constants.COLOUR_BACKGROUND);
		btn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
		});
		
		JPanel localJPanel = new JPanel();
	    localJPanel.add(btn);
	    localJPanel.setBorder(BorderFactory.createEtchedBorder());
	    localJPanel.setBackground(Constants.COLOUR_BACKGROUND);
	    return localJPanel;
	}
}

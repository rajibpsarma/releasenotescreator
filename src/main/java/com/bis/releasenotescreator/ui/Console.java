package com.bis.releasenotescreator.ui;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.bis.releasenotescreator.util.Constants;

/**
 * A class representing the console where various informations are written into and are displayed to user.
 * @author RSarma
 */
public class Console {
	private static Console instance = null;
	private void Console(){
	}
	public static Console getInstance() {
		if(instance == null) {
			instance = new Console();
			instance.init();
		}
		return instance;
	}
	
	// The main container that holds all other elements in it.
	private JPanel consoleUserInterface = new JPanel();
	// The text area where informations are written into
	private JTextArea consoleScreen;
	
	/**
	 * Some text is written to the console, which gets displayed to the user.
	 * @param text
	 */
	public void write(String text) {
		String msg = " ";
		if(consoleScreen.getText().isEmpty()) {
			msg = " " + text;
		}
		else {
			msg = consoleScreen.getText() + "\n " + text;
		}
		consoleScreen.setText(msg);
	}
	
	/**
	 * Clears the console.
	 */
	public void clear() {
		consoleScreen.setText("");
	}
	
	/**
	 * Returns the user interface 
	 * @return The user interface 
	 */
	public JPanel getUserInterface() {
		return consoleUserInterface;
	}
	
	private void init() {
		Box localBox2 = Box.createVerticalBox();
		Box btnBox = Box.createHorizontalBox();
		localBox2.add(getDisplayTextArea());
		localBox2.add(new JLabel("   ")); // Some empty space
		btnBox.add(getClearButton());
		btnBox.add(new JLabel("   ")); // Some empty space between the buttons
		btnBox.add(getCopyButton());
		localBox2.add(btnBox);
		consoleUserInterface.add(localBox2);
		
		consoleUserInterface.setBorder(BorderFactory.createEtchedBorder());
		consoleUserInterface.setBackground(Constants.COLOUR_BACKGROUND);
		
		write("Release Notes Creator, developed by Rajib Sarma");
	}
	
	private Box getDisplayTextArea() {
	    Box localBox = Box.createVerticalBox();
	    this.consoleScreen = new JTextArea(13, 57);
	    this.consoleScreen.setEnabled(false);
	    this.consoleScreen.setFont(Constants.FONT_CONSOLE);
	    this.consoleScreen.setBackground(Color.BLACK);
	    this.consoleScreen.setForeground(Color.white);
	    JScrollPane localJScrollPane = new JScrollPane(this.consoleScreen);
	    localBox.add(localJScrollPane);
	    
	    return localBox;
	}
	
	private Box getClearButton() {
		Box localBox = Box.createHorizontalBox();
		JButton localJButton = new JButton("Clear");
		localJButton.setFont(Constants.FONT_NORMAL);
		localJButton.setForeground(Color.black);
		localJButton.setFocusable(false);
		localJButton.setToolTipText("Clears the console");
		localJButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent paramAnonymousActionEvent) {
				clear();
			}
		});
		localBox.add(localJButton);
		return localBox;
	}
	
	private Box getCopyButton() {
		Box localBox = Box.createHorizontalBox();
		JButton localJButton = new JButton("Copy");
		localJButton.setFont(Constants.FONT_NORMAL);
		localJButton.setForeground(Color.black);
		localJButton.setFocusable(false);
		localJButton.setToolTipText("Copies the contents of the console to clipboard");
		localJButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent paramAnonymousActionEvent) {
				StringSelection stringSelection = new StringSelection (consoleScreen.getText());
				Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
				clpbrd.setContents (stringSelection, null);
			}
		});
		localBox.add(localJButton);
		return localBox;
	}	
}
